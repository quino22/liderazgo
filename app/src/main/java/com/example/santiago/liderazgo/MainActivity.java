package com.example.santiago.liderazgo;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity {
    public TextView txtNombre;
    public TextView txtApellido;
    public TextView txtDni;
    public TextView lblNombre;
    public TextView lblApellido;
    public TextView lblDni;
    private EditText edit;
    int i =0;

    List<Map<String,String>> alumnos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre= (TextView) findViewById(R.id.txtNombre);
        txtApellido = (TextView) findViewById(R.id.txtApellido);
        txtDni = (TextView) findViewById(R.id.txtDni);
        lblNombre= (TextView) findViewById(R.id.lblnombre);
        lblApellido = (TextView) findViewById(R.id.lblApellido);
        lblDni = (TextView) findViewById(R.id.lblDni);
        edit = (EditText) findViewById(R.id.editText);
        alumnos = new ArrayList<>();
    }



    public void readWebpage(View view) {
        //System.out.print("Buscando");
        i = 0;
        for (Map<String,String> a : alumnos) {
            System.out.println(a.get("dni").toString());
            System.out.println(edit.getText().toString());
            System.out.println(a.get("dni").toString().equals(edit.getText().toString()));
            if (a.get("dni").toString().equals(edit.getText().toString())){
                i = 1;
                System.out.println("Entro i ="+i);
                lblNombre.setText("Nombre:");
                lblApellido.setText("Apellido:");
                lblDni.setText("Dni:");
                txtNombre.setText(a.get("nombre") );
                txtApellido.setText(a.get("apellido"));
                txtDni.setText(a.get("dni"));
                return;
            }
        }
        if(i==0) {
            DownloadWebPageTask task = new DownloadWebPageTask();
            task.execute(new String[]{"https://virtual.evolutionsoluciones.com/main.php?id=" + edit.getText()});
        }

    }

    private class DownloadWebPageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            HttpURLConnection urlConnection = null;
            String result = "";
            try {
                URL url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int code = urlConnection.getResponseCode();

                if(code==200){
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    if (in != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                        String line = "";

                        while ((line = bufferedReader.readLine()) != null)
                            result += line;
                    }
                    in.close();
                }



                return result;

            } catch (MalformedURLException e) {
                System.out.println("JSON");
                e.printStackTrace();
            } catch (IOException e) {
                //restaurar(edit.getText().toString(), alumnos);
                //e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            //System.out.println("Encontro");
            return result;

        }

        @Override
        protected void onPostExecute(String result) {

            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(result);

                String dni = jsonObj.getString("dni");
                String nombre  = jsonObj.getString("nombre");
                String apellido  = jsonObj.getString("apellido");
                lblNombre.setText("Nombre:");
                lblApellido.setText("Apellido:");
                lblDni.setText("Dni:");
                txtNombre.setText(nombre);
                txtApellido.setText(apellido);
                txtDni.setText(dni);
                Map<String,String> alumno = new HashMap<>();
                alumno.put("dni",dni);
                alumno.put("nombre",nombre);
                alumno.put("apellido",apellido);
                alumnos.add(alumno);

            } catch (JSONException e) {
                System.out.println("Error");
                System.out.println(result);
                if(i ==0) {
                    System.out.println("Limpiar");
                    lblNombre.setText(result);
                    lblApellido.setText("");
                    lblDni.setText("");
                    txtNombre.setText("");
                    txtApellido.setText("");
                    txtDni.setText("");
                }
            }

            //textView.setText(Html.fromHtml(result));
        }

        public void restaurar(String dni, List<Map<String,String>> alumnos){
            if(!alumnos.isEmpty()) {
                for (Map<String, String> alumno : alumnos) {
                    String dniValue = alumno.get("dni");
                    String nombre = alumno.get("nombre");
                    String apellido = alumno.get("apellido");
                    if(dniValue.equals(dni)){

                        //System.out.println("nombre " + nombre);
                        //System.out.println("apellido " +apellido);
                        //System.out.println("dni "+dni);
                        try {
                            txtNombre.setText(nombre);
                            txtApellido.setText(apellido);
                            txtDni.setText(dniValue);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    }


                }
            }
        }
    }
}
